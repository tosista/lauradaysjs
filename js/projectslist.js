const projects = [
  {
    id: 1,
    title: 'Looney Tunes',
    brand: 'Max&Co',
    description: 'Young people going to a concert. They get lost, so they improvise a concert in the nature',
    color: 'tomato',
    featured: true,
    image: './images/maxco_looney.gif',
    video: '869197593'
  },
  {
    id: 2,
    title: 'Le Pettegole',
    brand: 'Cavalli',
    description: 'A wild night generates a lot of gossip',
    color: 'yellow',
    featured: true,
    image: './images/cavalli_pettegole.gif',
    video: '822926380'
  },
  {
    id: 3,
    title: 'Plastic Bag',
    brand: 'Ed Sheeran',
    description: 'Descrizione del progetto 3',
    color: 'lightblue',
    featured: true,
    image: './images/edsheeran.gif',
    video: '889847750'
  },
  {
    id: 4,
    title: 'Cosmoprof',
    brand: 'Cosmoprof',
    description: 'Launch teaser for new brand identity of Cosmoprof fairs',
    color: 'purple',
    featured: true,
    image: './images/cosmoprof.gif',
    video: '808668823'
  },
  {
    id: 5,
    title: 'Giorgio Armani FW22-23',
    brand: 'Giorgio Armani',
    description: 'Fotoshoot backstage of Armani\'s fall-winter collection',
    color: 'pink',
    featured: true,
    image: './images/armani.gif',
    video: '733267374'
  },
  {
    id: 6,
    title: 'Style Edit ATL',
    brand: 'BOSS',
    description: 'Fashion interviews to cultural influencers in Atlanta, Georgia',
    color: 'brown',
    featured: true,
    image: './images/boss.gif',
    video: '811206003'
  },
  {
    id: 7,
    title: 'UGG x Shuting',
    brand: 'Shuting',
    description: '',
    color: 'violet',
    featured: false,
    image: './images/ugg-small.gif',
    video: '773705808'
  },
  {
    id: 8,
    title: 'Sella Online Banking',
    brand: 'Banca Sella',
    description: '',
    color: 'blue',
    featured: false,
    image: './images/banca-sella.gif',
    video: '871889171'
  },
  {
    id: 9,
    title: 'A Tiny Love Story [short film]',
    brand: 'Short Film',
    description: '',
    color: 'red',
    featured: false,
    image: './images/ATLS.gif',
    video: '847083559'
  },
  {
    id: 10,
    title: 'Tod\'s',
    brand: 'Short Film',
    description: '',
    color: 'red',
    featured: false,
    image: './images/tods.gif',
    video: '838007961'
  },
  {
    id: 11,
    title: 'Origami all\' alba',
    brand: 'Clara',
    description: '',
    color: 'red',
    featured: false,
    image: './images/origami.gif',
    video: '817977048'
  },
];
