// this will contain the code that fetches the list of projects and renders the proper html
const params = new URLSearchParams(document.location.search);

let id = params.get('id');
let projectElement = projects.find((project) => project.id == id);
console.log(id)
console.log(projectElement)

let project = document.querySelector('#project-div');

project.innerHTML = `
<iframe src="https\://player.vimeo.com/video/${projectElement.video}?autoplay=1&loop=1&autopause=0&controls=0&muted=0" frameborder="0" allow="autoplay; fullscreen; picture-in-picture" allowfullscreen></iframe>
<h1>${projectElement.title}</h1>
<div class="project-info">
  <a id="prev-link"><img src="../images/animbuttons/Prev-small.gif" /></a>
  <p>client: ${projectElement.brand}</p>
  <p>directed by: Laura Days</p>
  <a id="next-link"><img src="../images/animbuttons/Next-small.gif" /></a>
</div>
`;

// DOM elements for previous and next link
let prevLink = document.querySelector('#prev-link')
let nextLink = document.querySelector('#next-link')

// function that goes to previous project
function prevLinkClick() {
  if (projectElement == projects[0]) {
    window.location.href = `project.html?id=${projects[projects.length - 1].id}`;
  } else {
    window.location.href = `project.html?id=${projectElement.id - 1}`;
  }
}

// function that goes to next project
function nextLinkClick() {
  if (projectElement == projects[projects.length - 1]) {
    window.location.href = `project.html?id=${projects[0].id}`;
  } else {
    window.location.href = `project.html?id=${projectElement.id + 1}`;
  }
}


// Event listeners for prev and next links
prevLink.addEventListener('click', prevLinkClick)

nextLink.addEventListener('click', nextLinkClick)

