// WORKS Page JS

// DOM selectors
const main = document.querySelector('#main-container')

// cycle through the projects list
projects.forEach((project) => {
  let workDiv = document.createElement('div')
  workDiv.setAttribute('class', 'work-container')
  workDiv.innerHTML = `
      <a href="project.html?id=${project.id}">
        <img src="${project.image}" alt="${project.title}">
        <h3>${project.title}</h3>
      </a>
  `
  main.appendChild(workDiv)
})
