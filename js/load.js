// Script that manages the loader status
// it waits for the window loading 

// DOM elements
let loadContainer = document.querySelector('.load-container')

// function that switches the loader off
function loaderOff(){
  loadContainer.style.display = 'none'
}

// event listener for window loaded
window.addEventListener('load', loaderOff)

