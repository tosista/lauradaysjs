// this will contain the main page script

console.log(projects[0].title);

let projectsList = document.querySelector('#projects-list');
let projectDisplay = document.querySelector('#project-display');
let tvImg = document.querySelector('#tv-img');
let reelImg = document.querySelector('#reel-img');

projects.filter(project => project.featured).forEach((project) => {
  // Create a new list item and insert an H3 with the project title
  let li = document.createElement('li');
  li.innerHTML = `<h3>${project.brand}</h3>`;

  // create the project display and insert the content
  let display = document.createElement('div')
  display.innerHTML = `<img src="${project.image}" class="project-img">`
  // hide the display by default
  display.style.display = 'none'

  // Append the created project li to the projects list
  projectsList.appendChild(li);

  // Append the created display to the project display
  projectDisplay.appendChild(display)

  // Add event listener to redirect to detail project page
  li.addEventListener('click', () => {
    window.location.href = `project.html?id=${project.id}`;
  });

  // Adding event listeners for displaying project showcase
  li.addEventListener('mouseover', () => {
    display.style.display = 'block'
  })
  li.addEventListener('mouseout', () => {
    display.style.display = 'none'
  })
});

// Add event listener to tv image, so the reel gif shows on mouse over
tvImg.addEventListener('mouseover', () => {
  reelImg.style.display = 'block'
})
// Adding event listener to tv image, to hide the reel on mouseout
tvImg.addEventListener('mouseout', ()=> {
  reelImg.style.display = 'none';
})
